# Task Custom Hook - React App (Platform Electives Objective)
A simple React App that allows users add tasks to do through an input form, and be stored into the backend database Firebase. The main objective that was demonstrated in this app was using Custom Hooks.

## Setup

```
npm install
npm run start
```
